import time

from instagram_scraper.app import InstagramScraper
from instagrapi import Client


INST_LOGINS = [
# "ronfederetocebiyatenkiva:qcpLqVT8D%xi74T",
# "kelyshnicynelyusova:baKs9ORY8",
"drushchakevlikiv:pWAg4pTf",
# "gaigradskiv:MJf7?g#5",
"kelentyrskijguzneva:TNuNc~kw",
"kapajforastctupkiv:J}rqDPx?",
"bionyshepochechutina:w89V1QZ3",
# "rereshchenropoedinceva:$i4CrBWf",
# "dubinamutcev:i3AXyv3N",
"kepalaskislgubiv:K8jk60uy",
]

INST_LOGINS = [ll.split(':') for ll in INST_LOGINS]

class InstLoaders:

    def __init__(self):

        self.loaders = []
        self.num = -1

        for ll in INST_LOGINS:
            print(ll)
            time.sleep(5)
            loader = InstagramScraper(profile_metadata=True, login_user=ll[0], login_pass=ll[1])
            loader.authenticate_with_login()
            self.loaders.append(loader)

    def get(self):
        self.num += 1
        print("loader num", self.num % len(self.loaders))
        return self.loaders[self.num % len(self.loaders)]


class GetClient:
    def __init__(self):
        self.clients = []
        self.num = 0
        for ll in INST_LOGINS:
            cl = Client()
            print(ll)
            time.sleep(5)
            cl.login(ll[0], ll[1])
            self.clients.append(cl)

    def get(self):
        self.num += 1
        return self.clients[self.num % len(self.clients)]