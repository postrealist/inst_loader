import json
import operator
import os
from collections import defaultdict

from emoji import distinct_emoji_lis, demojize, emoji_lis


def collect_users():
    data = []
    arr = os.listdir("/home/nazar/PycharmProjects/inst_users_2")
    for f in arr:
        sub_arr = os.listdir(f"/home/nazar/PycharmProjects/inst_users_2/{f}")
        for ff in sub_arr:
            path = f"/home/nazar/PycharmProjects/inst_users_2/{f}/{ff}"
            with open(path) as json_file:
                user_data = json.load(json_file)
                data.append(user_data)

    return data

def emo_list():
    dd = {}
    for line in open("emo_list.txt"):
        dd[line.split(",")[0]] = line.split(",")[1].replace("\n", "")

    return dd


if __name__ == "__main__":

    emos = emo_list()
    print(emos)

