from nltk.tokenize import RegexpTokenizer
import re
import Stemmer
import emoji

cleanr_extra = re.compile('[^a-zA-Zа-яА-Я0-9\-\_\.\@\#\$\%]')

cleanr = re.compile('[^a-zA-Zа-яА-Я0-9\-]')
en_stem = Stemmer.Stemmer('english')
ru_stem = Stemmer.Stemmer('russian')

def findWords(s: str):
    return s.split(" ")


def removeTrash(s: str):
    cleantext = re.sub(cleanr, ' ', s)
    return cleantext.replace("  ", " ").replace("  ", " ").replace('\n', "")


def tok(s: str):
   s1 = emoji.demojize(s)
   ws = findWords(removeTrash(s1).lower())
   return [stem(w) for w in ws if len(w) > 1]


def tok_extra(s: str):
   s1 = emoji.demojize(s)
   cleantext = re.sub(cleanr_extra, ' ', s1)
   return cleantext.replace('\n', " ").replace("  ", " ").replace("  ", " ").strip().lower()


def stem(s: str):
    w = ru_stem.stemWord(s)
    if w == s:
        return en_stem.stemWord(s)
    else:
        return w


class BigramsExtractor:

  @staticmethod
  def toBigrams(words):
      res = []
      for i in range(1, len(words)):
          res.extend(BigramsExtractor.extractBiGramsForPosition(words, i))
      return res

  @staticmethod
  def extractBiGramsForPosition(words, pos):
    curWord = words[pos]
    if pos > 1:
        return [f"{words[pos - 2]}_{curWord}", f"{words[pos - 1]}_{curWord}", f"{curWord}_{words[pos - 1]}"]
    elif pos > 0:
        return [f"{words[pos - 1]}_{curWord}", f"{curWord}_{words[pos - 1]}"]
    else:
        return []



if __name__ == "__main__":

    s = "💥Продюсер 💥Актёр 💥Ведущий  💥Реклама     💥Кино   💥Клипы  🎬🎤🎼 +79255006958"
    ds = emoji.demojize(s)
    print(ds)