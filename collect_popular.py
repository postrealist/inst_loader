import operator
import pickle
import time
import random
from collections import defaultdict

from clients import GetClient

a_file = open("/home/nazar/Downloads/uids4.npy", "rb")
uids = pickle.load(a_file)
print("ids len =", len(uids))


a_file = open("/home/nazar/Downloads/unames4.npy", "rb")
u_names = pickle.load(a_file)
print("names len =", len(u_names))

topchik = []

for k, v in u_names.items():
    if v > 1:
        topchik.append(k)

print(len(topchik))

get_clients = GetClient()


def update_ids(uid):

    following = get_clients.get().user_following(uid)
    print("groups", len(following))
    foll_set = {v.username: v for v in following.values()}
    if len(foll_set.keys() & u_names.keys()) < 4 * len(foll_set) // 5:
        for v in foll_set.values():
            u_names[v.username] += 1

id_list = list(uids.keys())
id_list.reverse()
random.shuffle(id_list)

it = 0
for id in id_list:
    it += 1
    time.sleep(10)
    print(it, id, len(u_names))
    try:
        update_ids(id)
    except:
        pass

    top = dict(sorted(u_names.items(), key=operator.itemgetter(1), reverse=True)[:100])
    print(top)

    if it % 100 == 0:
        a_file = open("/home/nazar/Downloads/unames5.npy", "wb")
        pickle.dump(u_names, a_file)
        a_file.close()

