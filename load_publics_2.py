import codecs
import json
import operator
import pickle
import time
import random
import io
import os
from igramscraper.model import Account
from clients import InstLoaders
from instagram_scraper import InstagramScraper
from igramscraper.instagram import Instagram


def save_json(data, dst='./'):
    """Saves the data to a json file."""
    if not os.path.exists(os.path.dirname(dst)):
        os.makedirs(os.path.dirname(dst))

    if data:
        output_list = {}
        if os.path.exists(dst):
            with open(dst, "rb") as f:
                output_list.update(json.load(codecs.getreader('utf-8')(f)))

        with open(dst, 'wb') as f:
            output_list.update(data)
            json.dump(output_list, codecs.getwriter('utf-8')(f), indent=4, sort_keys=True, ensure_ascii=False)


# If account is public you can query Instagram without auth
instagram = Instagram()

a_file = open("/home/nazar/Downloads/unames4.npy", "rb")
uids = pickle.load(a_file)
print(len(uids))
top_ids = dict(sorted(uids.items(), key=operator.itemgetter(1), reverse=True))


it = 0
for name, count in top_ids.items():
    it += 1
    if it < 0:
        continue
    time.sleep(3)

    user_info: Account = instagram.get_account(name)

    if user_info is None:
        continue

    if user_info.biography is None:
        user_info.biography = ""

    if user_info.business_category_name is not None:
        user_info.business_category_name = user_info.business_category_name.replace('/', ' ')

    if user_info.category_name is not None:
        user_info.category_name = user_info.category_name.replace('/', ' ')

    profile_info = {
        'biography': user_info.biography.replace('\n', ' ').replace('\r', ' '),
        'followers_count': user_info.followed_by_count,
        'following_count': user_info.follows_count,
        'full_name': user_info.full_name,
        'id': user_info.identifier,
        'is_business_account': user_info.is_business_account,
        'is_joined_recently': user_info.is_joined_recently,
        'is_private': user_info.is_private,
        'posts_count': user_info.media_count,
        'profile_pic_url': user_info.profile_pic_url,
        'external_url': user_info.external_url,
        'business_category_name': user_info.business_category_name,
        'category_name': user_info.category_name
    }

    key = f"{profile_info['category_name']}__{profile_info['business_category_name']}"

    print(it, profile_info['full_name'], "|", key, "|", profile_info['biography'])

    path = f"/home/nazar/PycharmProjects/inst_users_3/{key}/{name}.json"
    save_json(profile_info, path)

