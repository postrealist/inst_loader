import json
import math
import operator
import os
import shutil
import pandas as pd

from tok import tok, BigramsExtractor, tok_extra

arr = os.listdir("/home/nazar/PycharmProjects/inst_users_2")

dd = {}

for f in arr:
        sub_arr = os.listdir(f"/home/nazar/PycharmProjects/inst_users_2/{f}")
        # print(f, len(sub_arr))
        if len(sub_arr) > 4:
            dd[f] = len(sub_arr)

top_ids = dict(sorted(dd.items(), key=operator.itemgetter(1), reverse=True))

print(len(top_ids))


kws = pd.read_csv("/home/nazar/Downloads/keywords.csv")
print(len(kws))

kw_list = []

for i in range(len(kws)):
    kw_list.append({})
    kw_list[i]["title"] = kws.loc[i]["title"]
    kw_list[i]["key_words"] = set(str(kws.loc[i]["key_words"]).replace("[", "").replace("\"", "").replace("]", "").split(", "))
    kw_list[i]["tfidf_features"] = set(str(kws.loc[i]["tfidf_features"]).replace("[", "").replace("\"", "").replace("]", "").split(", "))
    kw_list[i]["tfidf_features_bigrams"] = set(
        str(kws.loc[i]["tfidf_features_bigrams"]).replace("[", "").replace("\"", "").replace("]", "").split(", "))


def find_max_sim(text: str):

    tokk = tok(text)
    # print(tokk)
    tok_set = set(tokk)
    # print(tok_set)
    mx = -1
    ind = None
    for i, d in enumerate(kw_list):
        sim1 = len(tok_set & d["key_words"])
        sim2 = len(tok_set & d["tfidf_features"]) / math.sqrt(len(d["tfidf_features"]) + 1)
        sim = 1.0 * sim1 + 2 * sim2
        # print(sim, d)
        if sim > mx:
            mx = sim
            ind = i

    return ind, mx, kw_list[ind]["title"]


cats = pd.read_csv("/home/nazar/Downloads/Telegram Desktop/nazar_ cats.txt")
cat_rank = {}
for i in range(len(cats)):
    cat_rank[str(cats.loc[i]["name"]).strip()] = int(cats.loc[i]["rank"])


good_cat_names = set(pd.read_csv("/home/nazar/PycharmProjects/instagram/t.txt").values.flatten().tolist())
print(good_cat_names)



cc = 0
final_data_set = {}

def extract_text(user):
    cat_key = f"{user['category_name']}__{user['business_category_name']}".replace("/", " ").replace(",", "")
    rank = cat_rank[cat_key] if cat_key in cat_rank else 1
    if rank < 3:
        return (user["full_name"] + " " + user["biography"]).replace("/", " ").replace(",", " ").replace("\n", " ")
    else:
        res = user["full_name"] + " "
        if user['category_name'] is not None and user['category_name'].strip() in good_cat_names:
            res += user['category_name'].replace("/", " ").replace(",", " ") + " "
        if user['business_category_name'] is not None and user['business_category_name'].strip() in good_cat_names:
            res += user['business_category_name'].replace("/", " ").replace(",", " ") + " "

        res += user["biography"]
        res = res.replace("/", " ").replace(",", " ").replace("\n", " ")
        # print(res)

        return res


for name, count in top_ids.items():
    print(name)
    sub_arr = os.listdir(f"/home/nazar/PycharmProjects/inst_users_2/{name}")
    for ff in sub_arr:
        with open(f"/home/nazar/PycharmProjects/inst_users_2/{name}/{ff}") as json_file:
            user_data = json.load(json_file)
            text_data = extract_text(user_data)
            ind, sim, title = find_max_sim(text_data)
            cat_key = f"{user_data['category_name']}__{user_data['business_category_name']}".replace("/", " ").replace(",", "")
            rank = cat_rank[cat_key] if cat_key in cat_rank else 1
            sim += (rank - 1) / 2
            if sim > 1.2:
                cc += 1
                final_data_set[text_data] = (sim, title)

print(cc)

top_ids = dict(sorted(final_data_set.items(), key=operator.itemgetter(1), reverse=True)[:10000])

dataset = open("/home/nazar/PycharmProjects/inst_data.txt", "w")

k = 0
for tt, sim in top_ids.items():
    k += 1
    tt = tok_extra(tt)
    print(k, sim[1], sim[0], str(tt)[:100])
    dataset.write(tt + "\n")




