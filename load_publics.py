import json
import operator
import pickle
import time
import random
import io
import os
from clients import InstLoaders
from instagram_scraper import InstagramScraper


def collect_names():
    arr = os.listdir("/home/nazar/PycharmProjects/inst_users_2")
    names = set()
    for f in arr:
        sub_arr = os.listdir(f"/home/nazar/PycharmProjects/inst_users_2/{f}")
        for ff in sub_arr:
            names.add(ff.replace(".json", "").strip())

    return names


saves_names = collect_names()

START_NUM = 0

a_file = open("/home/nazar/Downloads/unames4.npy", "rb")
uids = pickle.load(a_file)
print(len(uids))

top_ids = dict(sorted(uids.items(), key=operator.itemgetter(1), reverse=True))
loaders = InstLoaders()

it = 0
for name, count in top_ids.items():
    it += 1
    if it < START_NUM or name in saves_names:
        continue

    time.sleep(10)
    if it % 10 == 0:
        time.sleep(30)

    loader = loaders.get()

    user_info = None
    try:
        user_info = loader.get_shared_data_userinfo(name)
    except Exception as e:
        print("error", e)

    if user_info is None:
        continue

    if user_info['biography'] is None:
        user_info['biography'] = ""

    if user_info['business_category_name'] is not None:
        user_info['business_category_name'] = user_info['business_category_name'].replace('/', ' ')

    if user_info['category_name'] is not None:
        user_info['category_name'] = user_info['category_name'].replace('/', ' ')

    profile_info = {
        'biography': user_info['biography'].replace('\n', ' ').replace('\r', ' '),
        'followers_count': user_info['edge_followed_by']['count'],
        'following_count': user_info['edge_follow']['count'],
        'full_name': user_info['full_name'],
        'id': user_info['id'],
        'is_business_account': user_info['is_business_account'],
        'is_joined_recently': user_info['is_joined_recently'],
        'is_private': user_info['is_private'],
        'posts_count': user_info['edge_owner_to_timeline_media']['count'],
        'profile_pic_url': user_info['profile_pic_url'],
        'external_url': user_info['external_url'],
        'business_category_name': user_info['business_category_name'],
        'category_name': user_info['category_name']
    }

    key = f"{profile_info['category_name']}__{profile_info['business_category_name']}"

    print(it, count, profile_info['full_name'], "|", key, "|", profile_info['biography'])

    path = f"/home/nazar/PycharmProjects/inst_users_2/{key}/{name}.json"
    loader.save_json(profile_info, path)

